package twpathashala51.assignments;

import static org.junit.Assert.*;

public class CellTest {

    @org.junit.Test
    public void LiveCellWithlessThan2LiveNeighboursExpectDead()
    {
        int noOfLiveNeighbours = 1;
        String state= "dead";
        assertEquals(state, new Cell("live",noOfLiveNeighbours).newState());
    }

    @org.junit.Test
    public void liveCellWithMoreThanThreeLiveNeighboursExpectDead()
    {
        int noOfLiveNeighbours = 4;
        String state= "dead";
        assertEquals(state, new Cell("live",noOfLiveNeighbours).newState());
    }
    @org.junit.Test
    public void liveCellWithTwoLiveNeighboursExpectLive()
    {
        int noOfLiveNeighbours = 2;
        String state= "live";
        assertEquals(state, new Cell("live",noOfLiveNeighbours).newState());
    }
    @org.junit.Test
    public void liveCellWithThreeLiveNeighboursExpectLive()
    {
        int noOfLiveNeighbours = 2;
        String state= "live";
        assertEquals(state, new Cell("live",noOfLiveNeighbours).newState());
    }
    @org.junit.Test
    public void deadCellWithThreeLiveNeighboursExpectLive()
    {
        int noOfLiveNeighbours = 3;
        String state= "live";
        assertEquals(state, new Cell("dead",noOfLiveNeighbours).newState());
    }



}
