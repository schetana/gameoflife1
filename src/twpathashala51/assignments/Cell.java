package twpathashala51.assignments;

/**
 * Created by schetana on 7/23/16.
 */
public class Cell {

    int row;
    int column;
    private String state;
    private int noOfLiveNeighbours;

    public Cell(String state, int noOfLiveNeighbours) {
        row=0;
        column=0;
        this.state = state;
        this.noOfLiveNeighbours = noOfLiveNeighbours;
    }

    public String newState()
    {
        if(this.state =="live" && (noOfLiveNeighbours ==2 || noOfLiveNeighbours==3))
            return "live";
        if(this.state == "dead" && noOfLiveNeighbours==3)
            return "live";
        return "dead";
    }
}
