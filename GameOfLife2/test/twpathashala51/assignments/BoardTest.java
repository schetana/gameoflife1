package twpathashala51.assignments;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static twpathashala51.assignments.Cell.makeCell;
import static twpathashala51.assignments.Cell.state;

public class BoardTest {

    @Test
    public void InputIsBlockPatternOutputIsBlockPattern() {
        Set<Cell> listOfLiveCells = new HashSet<>();
        listOfLiveCells.add(makeCell(1, 1, state.LIVE));
        listOfLiveCells.add(makeCell(1, 2, state.LIVE));
        listOfLiveCells.add(makeCell(2, 1, state.LIVE));
        listOfLiveCells.add(makeCell(2, 2, state.LIVE));
        Board sampleBoard = Board.createBoard(listOfLiveCells);
        assertEquals(sampleBoard, sampleBoard.getNewPattern());
    }

    @Test
    public void checkingForNegativeScenarios() {
        Set<Cell> listOfLiveCells = new HashSet<>();
        listOfLiveCells.add(makeCell(1, 1, state.LIVE));
        listOfLiveCells.add(makeCell(1, 2, state.LIVE));
        listOfLiveCells.add(makeCell(2, 1, state.LIVE));
        listOfLiveCells.add(makeCell(2, 2, state.LIVE));
        Board sampleBoard = Board.createBoard(listOfLiveCells);
        Board expectedBoard = sampleBoard;
        expectedBoard.listOfLiveCells.add(makeCell(0, 0, state.LIVE));
        assertNotEquals(expectedBoard, sampleBoard.getNewPattern());
    }

    @Test
    public void checkInputAsBlinkerPattern() {
        Set<Cell> listOfLiveCells = new HashSet<>();
        Set<Cell> expectedListOfLiveCells = new HashSet<>();
        Cell c1 = makeCell(1, 1, state.LIVE);
        Cell c2 = makeCell(1, 0, state.LIVE);
        Cell c3 = makeCell(1, 2, state.LIVE);
        Cell c4 = makeCell(0, 1, state.LIVE);
        Cell c5 = makeCell(2, 1, state.LIVE);
        listOfLiveCells.add(c1);
        listOfLiveCells.add(c2);
        listOfLiveCells.add(c3);
        expectedListOfLiveCells.add(c1);
        expectedListOfLiveCells.add(c4);
        expectedListOfLiveCells.add(c5);
        Board sampleBoard = Board.createBoard(listOfLiveCells);
        Board expectedBoard = Board.createBoard(expectedListOfLiveCells);
        assertEquals(expectedBoard, sampleBoard.getNewPattern());
    }

    @Test
    public void checkInputAsBoatPattern() {
        Set<Cell> listOfLiveCells = new HashSet<>();
        Cell c1 = makeCell(0, 1, state.LIVE);
        Cell c2 = makeCell(1, 0, state.LIVE);
        Cell c3 = makeCell(2, 1, state.LIVE);
        Cell c4 = makeCell(0, 2, state.LIVE);
        Cell c5 = makeCell(1, 2, state.LIVE);
        listOfLiveCells.add(c1);
        listOfLiveCells.add(c2);
        listOfLiveCells.add(c3);
        listOfLiveCells.add(c4);
        listOfLiveCells.add(c5);
        Board sampleBoard = Board.createBoard(listOfLiveCells);
        Board expectedBoard = Board.createBoard(listOfLiveCells);
        assertEquals(expectedBoard, sampleBoard.getNewPattern());
    }

    @Test
    public void checkInputAsToadPattern() {
        Set<Cell> listOfLiveCells = new HashSet<>();
        Cell c1 = makeCell(1, 1, state.LIVE);
        Cell c2 = makeCell(1, 2, state.LIVE);
        Cell c3 = makeCell(1, 3, state.LIVE);
        Cell c4 = makeCell(2, 2, state.LIVE);
        Cell c5 = makeCell(2, 3, state.LIVE);
        Cell c6 = makeCell(2, 4, state.LIVE);
        listOfLiveCells.add(c1);
        listOfLiveCells.add(c2);
        listOfLiveCells.add(c3);
        listOfLiveCells.add(c4);
        listOfLiveCells.add(c5);
        listOfLiveCells.add(c6);
        Board sampleBoard = Board.createBoard(listOfLiveCells);
        Set<Cell> expectedListOfLiveCells = new HashSet<>();
        Cell o1 = makeCell(1, 1, state.LIVE);
        Cell o2 = makeCell(0, 2, state.LIVE);
        Cell o3 = makeCell(1, 4, state.LIVE);
        Cell o4 = makeCell(2, 1, state.LIVE);
        Cell o5 = makeCell(2, 4, state.LIVE);
        Cell o6 = makeCell(3, 3, state.LIVE);
        expectedListOfLiveCells.add(o1);
        expectedListOfLiveCells.add(o2);
        expectedListOfLiveCells.add(o3);
        expectedListOfLiveCells.add(o4);
        expectedListOfLiveCells.add(o5);
        expectedListOfLiveCells.add(o6);
        Board outputBoard = Board.createBoard(expectedListOfLiveCells);
        assertEquals(outputBoard, sampleBoard.getNewPattern());
    }

}
