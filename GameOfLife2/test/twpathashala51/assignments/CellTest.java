package twpathashala51.assignments;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static twpathashala51.assignments.Cell.makeCell;
import static twpathashala51.assignments.Cell.state;

public class CellTest {

    // Checking IsNeighbour() Method
    @Test
    public void zeroCommaZeroIsNeighbourOfOneCommaOne() {
        assertTrue(makeCell(1, 1, state.LIVE).hasNeighbourAs(makeCell(0, 0, state.LIVE)));
    }

    @Test
    public void zeroCommaOneIsNeighbourOfOneCommaOne() {
        assertTrue(makeCell(1, 1, state.LIVE).hasNeighbourAs(makeCell(0, 1, state.LIVE)));
    }

    @Test
    public void zeroCommaFourIsNotTheNeighbourOfOneCommaOne() {
        assertFalse(makeCell(1, 1, state.LIVE).hasNeighbourAs(makeCell(0, 4, state.LIVE)));
    }

    //    Checking CalculateNumberOfLiveCells() Method
    @Test
    public void calculateNumberOfLiveNeighboursForLiveCell() {
        Set<Cell> listOfLiveCells = new HashSet<>();
        Cell c1 = makeCell(1, 1, state.LIVE);
        listOfLiveCells.add(c1);
        listOfLiveCells.add(makeCell(1, 2, state.LIVE));
        listOfLiveCells.add(makeCell(2, 1, state.LIVE));
        listOfLiveCells.add(makeCell(2, 2, state.LIVE));
        Board sampleBoard = new Board(listOfLiveCells);
        assertEquals(3, c1.calculateNumberOfLiveNeighbours(sampleBoard));
    }

    @Test
    public void calculateNumberOfLiveNeighboursForDeadCell() {
        Set<Cell> listOfLiveCells = new HashSet<>();
        Cell c0 = makeCell(0, 0, state.DEAD);
        listOfLiveCells.add(makeCell(1, 1, state.LIVE));
        listOfLiveCells.add(makeCell(1, 2, state.LIVE));
        listOfLiveCells.add(makeCell(2, 1, state.LIVE));
        listOfLiveCells.add(makeCell(2, 2, state.LIVE));
        Board sampleBoard = new Board(listOfLiveCells);
        assertEquals(1, c0.calculateNumberOfLiveNeighbours(sampleBoard));
    }

    //checking if dead cells are coming alive or not
    @Test
    public void deadCellShouldComeAlive() {
        Set<Cell> listOfLiveCells = new HashSet<>();
        Cell c0 = makeCell(0, 0, state.LIVE);
        Cell c1 = makeCell(0, 1, state.LIVE);
        listOfLiveCells.add(c1);
        listOfLiveCells.add(makeCell(1, 0, state.LIVE));
        listOfLiveCells.add(makeCell(1, 1, state.LIVE));
        HashSet<Cell> expectedList = new HashSet<>();
        expectedList.add(c0);
        Board sampleBoard = new Board(listOfLiveCells);
        assertEquals(expectedList, c1.makingDeadCellsAlive(sampleBoard));
    }

}