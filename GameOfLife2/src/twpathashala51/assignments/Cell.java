package twpathashala51.assignments;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

//the basic unit of the game
public class Cell {

    private int row;
    private int column;
    private state stateOfCell;

    enum state {
        LIVE, DEAD
    }

    private Cell(int row, int column, state status) {
        this.row = row;
        this.column = column;
        this.stateOfCell = status;
    }

    public static Cell makeCell(int row, int column, state status) {
        return new Cell(row, column, status);
    }

    public Set<Cell> makingDeadCellsAlive(Board currentBoard) {
        Set<Cell> deadToLiveList = new HashSet<>();
        for (int row = this.row - 1; row <= this.row + 1; row++) {
            for (int column = this.column - 1; column <= this.column + 1; column++) {
                Cell tempCell = Cell.makeCell(row, column, state.LIVE);
                if (!currentBoard.listOfLiveCells.contains(tempCell)) {
                    int noOfLiveNeighbours = tempCell.calculateNumberOfLiveNeighbours(currentBoard) + 1;
                    if (noOfLiveNeighbours == 3) {
                        deadToLiveList.add(tempCell);
                    }
                }
            }
        }
        return deadToLiveList;
    }

    public int calculateNumberOfLiveNeighbours(Board current) {
        int liveNeighbourCount = 0;
        Iterator<Cell> iterator = current.listOfLiveCells.iterator();
        while (iterator.hasNext()) {
            Cell other = iterator.next();
            if (this.hasNeighbourAs(other)) {
                liveNeighbourCount++;
            }
        }
        return (this.stateOfCell.equals(state.LIVE) ? liveNeighbourCount - 1 : liveNeighbourCount);
    }

    public boolean hasNeighbourAs(Cell neighbour) {
        return (Math.abs(this.row - neighbour.row) <= 1 && Math.abs(this.column - neighbour.column) <= 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        if (row != cell.row) return false;
        if (column != cell.column) return false;
        return stateOfCell == cell.stateOfCell;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        result = 31 * result + (stateOfCell != null ? stateOfCell.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "("+row+", "+column+")";
    }
}




