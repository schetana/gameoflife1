package twpathashala51.assignments;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class GameOfLife {

    public static void main(String[] args) throws IOException {
        try {
            File file = new File("/Users/schetana/IdeaProjects/GameOfLife2/Input_File");
            FileReader fileReader = new FileReader(file);
            BufferedReader inputReader = new BufferedReader(fileReader);
            Set<Cell> liveCells = new HashSet<>();
            String line;
            while ((line = inputReader.readLine()) != null) {
                String[] coordinates = line.split(",");
                liveCells.add(Cell.makeCell(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]),
                        Cell.state.LIVE));
            }
            Board initialPattern = new Board(liveCells);
            Board finalPattern = initialPattern.getNewPattern();
            File outfile = new File("/Users/schetana/IdeaProjects/GameOfLife2/Output_File");
            FileWriter fileWriter = new FileWriter(outfile);
            BufferedWriter outputWriter = new BufferedWriter(fileWriter);
            outputWriter.write(finalPattern.toString());
            outputWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

