package twpathashala51.assignments;

import java.util.HashSet;
import java.util.Set;

// collection of cells
public class Board {

    public Set<Cell> listOfLiveCells;

    Board(Set<Cell> listOfLiveCells) {
        this.listOfLiveCells = listOfLiveCells;
    }

    public static Board createBoard(Set listOfLiveCells) {
        return new Board(listOfLiveCells);
    }

    public Board getNewPattern() {

        Set<Cell> outputList = new HashSet<>();
        for (Cell liveCell : this.listOfLiveCells) {
            int numberOfLiveNeighboursOfCurrentCell = liveCell.calculateNumberOfLiveNeighbours(this);
            if (numberOfLiveNeighboursOfCurrentCell == 2 || numberOfLiveNeighboursOfCurrentCell == 3) {
                outputList.add(liveCell);
            }
            outputList.addAll(liveCell.makingDeadCellsAlive(this));
        }
        return new Board(outputList);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Board board = (Board) o;
        return listOfLiveCells != null ? listOfLiveCells.equals(board.listOfLiveCells) : board.listOfLiveCells == null;

    }

    @Override
    public int hashCode() {
        return listOfLiveCells != null ? listOfLiveCells.hashCode() : 0;
    }

    @Override
    public String toString() {
        return ""+listOfLiveCells;
    }
}
