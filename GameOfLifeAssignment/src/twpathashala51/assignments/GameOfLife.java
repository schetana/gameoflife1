package twpathashala51.assignments;

//represents the board for the game
public class GameOfLife {

    int maxRowNumber;
    int maxColumnNumber;
    int[][] board;

    public GameOfLife(int maxRowNumber, int maxColumnNumber, int[][] board) {
        this.maxRowNumber = maxRowNumber;
        this.maxColumnNumber = maxColumnNumber;
        this.board = board;
    }

    private int calculateNoOfLiveNeighbours(int row, int column) {
        int noOfNeighbours = 0;
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = column - 1; j <= column + 1; j++) {
                if (checkNeighbourExists(i, j) && board[i][j] == 1)
                    noOfNeighbours++;
            }
        }
        return noOfNeighbours - board[row][column];

    }

    private boolean checkNeighbourExists(int row, int column) {
        return (row < maxRowNumber && row >= 0 && column < maxColumnNumber && column >= 0);

    }

    public int[][] newState() {
        int[][] nextStateOfBoard = new int[maxRowNumber][maxColumnNumber];
        try {
            for (int i = 0; i < maxRowNumber; i++) {
                for (int j = 0; j < maxColumnNumber; j++) {
                    int liveNeighbours = calculateNoOfLiveNeighbours(i, j);
                    if (board[i][j] == 1 && liveNeighbours < 2 || liveNeighbours > 3)
                        nextStateOfBoard[i][j] = 0;
                    else if (board[i][j] == 0 && liveNeighbours == 3)
                        nextStateOfBoard[i][j] = 1;
                    else
                        nextStateOfBoard[i][j] = board[i][j];
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return nextStateOfBoard;
    }
}


