package twpathashala51.assignments;

import org.junit.Test;

import static org.junit.Assert.*;

public class GameOfLifeTest {

    @Test
    public void checkOneLiveCellExpectDead() {
        int[][] sampleBoard = new int[][]{{1}};
        int[][] outputBoard = new int[][]{{0}};
        assertArrayEquals(outputBoard, new GameOfLife(1, 1, sampleBoard).newState());

    }

    @Test
    public void checkTwoLiveCellsExpectDead() {
        int[][] sampleBoard = new int[][]{{1, 1}};
        int[][] outputBoard = new int[][]{{0, 0}};
        assertArrayEquals(outputBoard, new GameOfLife(1, 2, sampleBoard).newState());

    }

    @Test
    public void checkFourCellsInARow() {
        int[][] sampleBoard = new int[][]{{0, 1, 1, 1}};
        int[][] outputBoard = new int[][]{{0, 0, 1, 0}};
        assertArrayEquals(outputBoard, new GameOfLife(1, 4, sampleBoard).newState());
    }

    @Test
    public void checkTwoRowsTwoColumns() {
        int[][] sampleBoard = new int[][]{{0, 1}, {1, 1}};
        int[][] outputBoard = new int[][]{{1, 1}, {1, 1}};
        assertArrayEquals(outputBoard, new GameOfLife(2, 2, sampleBoard).newState());
    }


    @Test
    public void checkBlockPatternExpectBlockPattern() {
        int[][] sampleBoard = new int[][]{{0, 0, 0}, {0, 1, 1}, {0, 1, 1}};
        int[][] outputBoard = new int[][]{{0, 0, 0}, {0, 1, 1}, {0, 1, 1}};
        assertArrayEquals(outputBoard, new GameOfLife(3, 3, sampleBoard).newState());
    }

    @Test
    public void checkBoatPatternExpectBoatPattern() {
        int[][] sampleBoard = new int[][]{{0, 1, 1}, {1, 0, 1}, {0, 1, 0}};
        int[][] outputBoard = new int[][]{{0, 1, 1}, {1, 0, 1}, {0, 1, 0}};
        assertArrayEquals(outputBoard, new GameOfLife(3, 3, sampleBoard).newState());
    }

    @Test
    public void checkBlinkerPattern() {
        int[][] sampleBoard = new int[][]{{0, 0, 0}, {1, 1, 1}, {0, 0, 0}};
        int[][] outputBoard = new int[][]{{0, 1, 0}, {0, 1, 0}, {0, 1, 0}};
        assertArrayEquals(outputBoard, new GameOfLife(3, 3, sampleBoard).newState());
    }

    @Test
    public void checkToadPattern() {
        int[][] sampleBoard = new int[][]{{0, 0, 0, 0, 0}, {0, 1, 1, 1, 0}, {0, 0, 1, 1, 1}, {0, 0, 0, 0, 0}};
        int[][] outputBoard = new int[][]{{0, 0, 1, 0, 0}, {0, 1, 0, 0, 1}, {0, 1, 0, 0, 1}, {0, 0, 0, 1, 0}};
        assertArrayEquals(outputBoard, new GameOfLife(4, 5, sampleBoard).newState());
    }

}
